-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 15, 2021 at 11:15 AM
-- Server version: 5.7.36
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arificia_indulkar`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `content`, `category`, `status`, `featured_image`, `folder`, `created_at`, `updated_at`) VALUES
(10, 'SHOULD YOU BUY A HOUSE IN 2021?', 'There are enough compelling reasons for those who have secure jobs. Post the pandemic, property valuations are at realistic levels, mortgage rates are at the lowest, some states have started reducing the stamp duty and developers are offering flexible payment schemes and GST waivers Snigdha Sharma, 38, works with an IT firm in Bengaluru. Thanks to the pandemic, she has been forced to work-from-home for the last eight months. Not sure when she is likely to be called back to work, she now spends her free time either researching on real estate projects or calling up brokers to fix an appointment for a site visit. COVID-19 has given her loads of time to rethink her investment decisions and buying a home in 2021 is one of her New Year wishes. So, are there enough compelling reasons for her to purchase a property now? The answer is yes since she seems to have a secure job and can afford it. This means that one should decide to go in for a house purchase only if one has enough savings to fund at least 30 percent of the cost of the apartment. Another reason is that post the pandemic property valuations today are at realistic levels, mortgage rates are at the lowest, some states have started reducing the stamp duty to make property purchase relatively affordable, developers are offering flexible payment schemes such as 10:90 and waiver of GST, stamp duty and other benefits. What this means is that post the pandemic, one is at least assured that one is not buying a property at ridiculously inflated prices. Home loan interest rates are at a 15-year low. Coupled with the bottomed-out property prices and additional discounts and offers by developers, there are very real savings to be secured on life\'s most cost-intensive investment. Housing affordability has improved in tandem with the times. To boost consumption, the RBI has brought lending rates down to a two-decade low and thrown several other sweeteners into the mix. In Maharashtra, stamp duty charges were brought down in August 2020. On their part, developers have not only trimmed prices to their lowest best, but also responded to the current exigencies by offering several cost-saving incentives. Moreover, serious buyers can negotiate the final price like never before. “Developers are doling out multiple deals and discounts to lure buyers along with the government. incentives (limited period-stamp duty cut in Maharashtra) leads to almost 5-15% reduction in overall cost acquisition. Developers will continue to fuel demand in 2021 as well,” said Anuj Puri, chairman – ANAROCK Property Consultants. There are also several options available in the market, even in ready-to-move properties. Moreover, prices of RTM properties are almost at par with under-construction homes in many areas. This has never happened before - and since developers have curtailed new supply, it is unlikely to happen again. Never have homebuyers had such a tempting choice range, at such low prices, he says. There are more compelling reasons. According to Anckur Srivasttava of GenReal Advisers, one should plan to buy a property within the next 100 days because real estate is unfortunately not about buying any unit but about a unit that you would like. “While three months down the line you may still find the unit, it may not tick all the boxes or the floor you prefer to be on or the Vastu direction or the window facing direction,” he says, adding due to the pandemic there are several buyer-favouring market circumstances and these factors are unlikely to repeat themselves. What should you buy? First, who should buy? Salaried people who wish to buy their first house. Those who already own a house can consider upgrading to a more spacious apartment due to the compulsion of working from home on account of the pandemic. Besides apartments, buyers could even consider buying into a plot by a Grade A developer. The top cities like Bengaluru, Hyderabad, Chennai, Pune and Gurugram are seeing high demand for plots in the wake of the new realities presented by COVID-19. Some of the leading developers with plotted developments now include DLF Ltd., Mahindra Lifespaces, Raheja Group, Godrej Properties, Century Real Estate, Puravankara’s Provident Housing, Shriram Properties, Goel Ganga, TVS and Alpha Corp, among others. The minimum size of marketable plots is as low as 550 sq. ft., and goes up to 10,000 sq. ft. In select projects, larger plots are also available. Plot sizes ranging between 1,200 - 2,500 sq. ft. are seeing maximum demand. However, in the southern cities of Bengaluru and Chennai, smaller plots of average sizes 550 - 750 sq. ft. are also generating interest. In the midst of the COVID-19 pandemic, demand for plots has gone up as homebuyers see this asset class as a low capital investment with limited project execution risks and faster exit opportunity while for developers it is a means to generate quick cash flows and a smart strategy to liquidate land banks for raising working capital. “Four floors with 12 bedrooms and large-sized living rooms on a single plot for Rs 2 crore offers a better investment proposition than a similarly priced apartment in a high-rise for which a buyer may have to wait longer for the project to get completed,” say real estate experts. The asset class requires minimal capital investment, limited project execution risks and faster exit opportunity, says Anuj Puri, chairman – ANAROCK Property Consultants. Builder floors are also more efficient than apartments. During COVID-19, several tenants residing in high-rises moved out to affordable builder floors where the maintenance costs are less. Therefore, builder floors are more ‘efficient’ than high-rise apartments as the common area is greatly reduced. The focus on dispersed offices and flexible workplace policies in the wake of the COVID-19 pandemic is expected to spur housing demand in the peripheries of cities, beyond the city centre hotspots due to which integrated townships may become popular among buyers. According to data made available by Anarock, the National Capital Region (NCR) has the maximum number of integrated townships - 42 projects with approximately 1.33 lakh units, followed by the Mumbai Metropolitan Region (MMR) with 17 projects having around 63,500 housing units, it said. “Clearly, this is a hugely underserved segment whose underpinning relevance and importance has been emphatically brought to light by the pandemic. During the COVID-19 pandemic, there is a strong rationale for living in integrated townships. These self-sustaining, compact urban ecosystems are now more than just lifestyle upgrades - they provide the kind of controlled environment that makes a big difference during such an outbreak,” says Puri. Things to keep in mind before buying a house A buyer should make sure that the title of the seller is clear and it should be free from encumbrances. In case of a secondary purchase, all the documents related to the property for a period of 30 years should be examined, if not 30 years then documents for a period of 12 years should be examined. In case of a new project, the layout plan should have been approved from municipal authorities. Occupancy certificate from the competent authority should be obtained before handing over the property. If this has not been obtained, there is a risk of the property getting demolished. There may also be penalties under building bye-laws of that area. Is a ready-to-move-in property better than an under construction one? Well, it certainly is as you do not have to pay both rent and EMI and it reduces the delay risks associated with a property that is still getting constructed. But is it not expensive? Not so in a market where there is huge inventory available and there are good deals available if you do your homework well. Also, too much inventory in the market has helped keep a check on prices. The project should also be registered under RERA and the buyer should verify if all provisions have been complied with. And most important, even if the project is RERA-registered, do not purchase property from a builder who is sitting on debt. Make sure you try and keep three to four options open and not get stuck to a single property. The golden rule is to explore. Out of the four properties you select, at least one seller in the resale market or developer will get back to you. The buyer should be willing to negotiate hard or to walk away. It’s advisable for buyers to choose the right location. See that there are proper roads leading up to the project, enough shops for daily needs, schools and hospitals are close by. Most importantly, check the distance to your workplace and the mode of transport available. Last but not least, remember you are buying a property during the pandemic for self-use, so don’t expect property prices to appreciate for the next two years.\r\n\r\nCard image\r\nCard image\r\nCard image\r\nExperts around the world have opined that real estate is an investment that will never go sour, and for good reason. With an excellent rate of returns and unparalleled wealth-building opportunities, real estate truly is a good investment. If you are wondering whether you should finally take the leap and invest in real estate projects in Pune then this article is for you. Let’s examine some of the biggest advantages to investing in real estate, and how, when done right, investment in real estate can be highly beneficial in the long run.\r\n\r\nWhether you own a commercial office space or a luxury apartment, the inherent value of real estate is that it is always counted as a tangible asset. These assets are flexible and can be converted to suit the need of the hour, generating rental revenue for you all year round. The property will always have value, giving you returns even if you decide to sell it. Definitely oOpt for home insurance and be sure to do your research, and pick the best policy for your needs, in order to keep your assets well protected.\r\n\r\nA large majority of people invest in real estate properties for the steady cash inflow that it generates through rental income. This passive income source serves as a strong incentive for people to invest in real estate property. Metropolitan cities or urban towns with good social infrastructure and connectivity to schools, colleges and IT hubs tend to reap a higher income for the owners as the demand is always high in such areas. Location is a key component to keep in mind when investing in real estate', '1', 'published', '1632908925.jpg', '/blogs', '2021-09-29 04:18:45', '2021-09-29 04:18:45'),
(11, 'ABOUT EVARA PROJECT', 'About Avalon Developers Evara II Avalon Developers Evara II is strategically located in Undri and is a well-planned project. It is spread over a sprawling area of 13988 Sq-m. There are in total 112 units in this project. It is a beautifully constructed Residential property that is sure to please you. The units are all Ready To Move. The different types of units available are Flat, which have been designed to offer complete satisfaction. The property units offer a comfortable space, and vary in size from 2 BHK Flat (1135. 0 Sq. Ft. - 1135. 0 Sq. Ft. ). The prices of this meticulously planned project lie in the range of Rs. 59. 0 Lac Onwards. This well-designed site has 2 towers, each with its own advantage. The project was launched in 01 February 2019. The commencement certificate of Avalon Developers Evara II has been granted. And the occupancy certificate is not granted. This project has been developed by the well-known builder Avalon Developers Group. At Avalon Developers Evara II you get to enjoy the best of facilities and amenities, such as Lift, Flower Gardens, Power Back Up, Piped Gas, Waste Disposal, Security, Gymnasium, Swimming Pool, Club House, Rain Water Harvesting. Its pincode is 411060. Enjoy the comforts of living in Avalon Developers Evara II with all modern conveniences at your disposal. Avalon Developers Evara II Floor Plan Floor plans are the perfect way to understand how the spaces/rooms of Avalon Developers Evara II are structured and help provide a clear picture of the house layout. The properties in Avalon Developers Evara II are available in 1 configurations and 1 varied layouts (or floor plans), with 2 BHK Flat present in 1135 sq. ft super area. All these configurations come with bathrooms and balconies. All the various configurations of Avalon Developers Evara II are available on Magicbricks for your reference. Avalon Developers Evara II Brochure To get detailed and comprehensive information about Avalon Developers Evara II you can download the brochure PDF. This brochure is available on the top right side of the page under the link \'Download Brochure\'. To view the Avalon Developers Evara II brochure PDF, fill in your details like name, contact number, and email ID. Project Advantage • 10 km from Station • 18 km from Airport • 5 km from Ruby Hall hospital • 3 km from Royal Heritage Mall • Less than 1 km from D.P.S. & a number of renowned schools in the radius of 3 kms viz. Bishops, Vibgyor, Caelum, RIMS, Euro Kids etc. • 500 mtrs from The Corinthians Resort and Club • 3 kms from The Royal Heritage Mall and Multiplex INOX USP: • The Most trusted brand in India by International Business Consultants (IBC) • The Best Brand in Pune by Pune Leadership Awards\r\n\r\nCard image\r\nCard image\r\nCard image\r\nExperts around the world have opined that real estate is an investment that will never go sour, and for good reason. With an excellent rate of returns and unparalleled wealth-building opportunities, real estate truly is a good investment. If you are wondering whether you should finally take the leap and invest in real estate projects in Pune then this article is for you. Let’s examine some of the biggest advantages to investing in real estate, and how, when done right, investment in real estate can be highly beneficial in the long run.\r\n\r\nWhether you own a commercial office space or a luxury apartment, the inherent value of real estate is that it is always counted as a tangible asset. These assets are flexible and can be converted to suit the need of the hour, generating rental revenue for you all year round. The property will always have value, giving you returns even if you decide to sell it. Definitely oOpt for home insurance and be sure to do your research, and pick the best policy for your needs, in order to keep your assets well protected.\r\n\r\nA large majority of people invest in real estate properties for the steady cash inflow that it generates through rental income. This passive income source serves as a strong incentive for people to invest in real estate property. Metropolitan cities or urban towns with good social infrastructure and connectivity to schools, colleges and IT hubs tend to reap a higher income for the owners as the demand is always high in such areas. Location is a key component to keep in mind when investing in real estate.\r\n\r\n', '3', 'published', '1632909032.jpg', '/blogs', '2021-09-29 04:20:32', '2021-09-29 04:20:32'),
(12, 'BUYERS’ GUIDE TO RESIDENTIAL PROPERTY IN 2021', 'BUYERS’ GUIDE TO RESIDENTIAL PROPERTY IN 2020\r\n\r\nEvery asset class, be it equities, mutual funds, gold or for that matter even real estate, experiences its share of volatility. However, that does not mean one should restrain from exploring them. All that is required is making a smart and informed decision while choosing wisely when building the portfolio of assets. As an asset class, real estate has its own charm. While buying an apartment as against renting or leasing is a never ending debate, both approaches have their benefits at different stages of life. The rationale behind paying a monthly rent and having the flexibility and freedom to move from one neighbourhood to another seems logical during early years. However, there always comes a time when owning a house outweighs leading a nomadic lifestyle. The very fact that the individual home loans segment has remained steady despite a challenging economic environment speaks volumes about the attractiveness of real estate as an asset class. Validating the trend are some of country’s leading private sector banks with higher double-digit year-on-year growth in their individual loans category during the September 2019 quarter. This, despite a larger home loans base in the corresponding period last fiscal. So, if you have already decided on the life goal of owning an apartment in 2020, I suggest start with understanding the art behind the science of buying a house before taking the plunge. Let me take you through some of the key aspects that will ensure a seamless experience. Financial Assessment Financial assessment is the first step in this journey and a very important one. Since, buying an apartment is among the most expensive transactions in an individual’s life, a proper financial assessment is very crucial. Compare your reserves and earnings with expenses, outstanding loans and other liabilities (if any) to get a clear picture about your finances. Checking with the bank, you already have an account in, and researching online for eligibility and a pre-approved home loan will give you a better understanding of the apartment sizes and price range. This exercise will also tell you the extent of own contribution required or to be organised for the transaction. Additionally, adding an earning family member and availing a joint home loan will help increase the eligibility. Property Search Location, scope of the project and development, connectivity, quality of life, existing and upcoming social infrastructure, pricing and rental trends, and future prospects are some of the key aspects to be looked at when conducting a property search. Start with secondary research gathering information about projects and developers in your identified location/ market through online resources, real estate forums, property exhibitions and even brokers. Of late groups onsocial media channels like Facebook, Twitter groups are playing a crucial role in gathering market intelligence as well. Assistance can be sought from your lending partner as most of them operate a property search vertical as a value added service. Follow this with primary research comprising a recce of the area and site visits to get a better understanding of the ground reality. This is also where financial assessment comes handy. Knowing the price range beforehand helps narrow the search and the type of the apartment (1BHK, 2BHK or 3BHK depending on your requirement) that can be looked at in the specified budget. The implementation GST and the Maharashtra Real Estate Regulatory Authority (MahaRERA) have, to a great extent, made it easier for potential buyers to find concrete details of projects being shortlisted in the location of choice. While market dynamics and competition has forced stand alone local developers to offer quality housing, going for projects undertaken by reputed real estate companies with a commendable accomplishment is always reassuring. Under Construction Or Ready To Move In Both types of properties have advantages and the buying decision depends on the buyers. An under- construction property also allows the buyer to look at a bigger apartment in the specified budget, in case there are more members in the family. While there are project completion risks associated with under construction developments, SBI’s newly-launched home buyer finance guarantee scheme could help mitigate it to a great extent. Opting for a ready to move in (RTMI) apartment offers more advantages though, even if you are planning to put it on rent as opposed to moving in with your family. One is able to closely examine the project offering, quality of construction, basic amenities and other lifestyle facilities that are part of the project in addition to the overall look and feel of the development. With an RTMI apartment, there are other advantages that an under construction property will not offer. With RTMI, you will save on GST and earn rental income (if you have bought it for that purpose) immediately after taking possession Funding Your Residential Property A home loan will typically give between 80 per cent and 85 per cent of the funds required for purchasing the apartment. You will have to bring in the balance 15 per cent to 20 per cent depending on the financial institution providing you the home loan. Provisions will have to be made for additional expenses to be incurred over and above the cost of the apartment being considered for purchase. If looking for a higher loan amount you can consider a top-up facility. Insuring Your Asset Financial institutions tend to bundle a home loan insurance product when funding your purchase and it is a good thing to have one. However, it is important understand the details of the insurance product and the purpose being served. Do make a cost – benefit analysis of the insurance offering and compare it with other products in the market before availing the scheme\r\n\r\nCard image\r\nCard image\r\nCard image\r\nExperts around the world have opined that real estate is an investment that will never go sour, and for good reason. With an excellent rate of returns and unparalleled wealth-building opportunities, real estate truly is a good investment. If you are wondering whether you should finally take the leap and invest in real estate projects in Pune then this article is for you. Let’s examine some of the biggest advantages to investing in real estate, and how, when done right, investment in real estate can be highly beneficial in the long run.\r\n\r\nWhether you own a commercial office space or a luxury apartment, the inherent value of real estate is that it is always counted as a tangible asset. These assets are flexible and can be converted to suit the need of the hour, generating rental revenue for you all year round. The property will always have value, giving you returns even if you decide to sell it. Definitely oOpt for home insurance and be sure to do your research, and pick the best policy for your needs, in order to keep your assets well protected.\r\n\r\nA large majority of people invest in real estate properties for the steady cash inflow that it generates through rental income. This passive income source serves as a strong incentive for people to invest in real estate property. Metropolitan cities or urban towns with good social infrastructure and connectivity to schools, colleges and IT hubs tend to reap a higher income for the owners as the demand is always high in such areas. Location is a key component to keep in mind when investing in real estate.', '1', 'published', '1632909124.png', '/blogs', '2021-09-29 04:22:04', '2021-09-29 04:22:04'),
(13, 'WHAT ARE THE DOCUMENTS YOU WOULD NEED FOR BEFORE BUYING A PROPERTY', 'WHAT ARE THE DOCUMENTS YOU WOULD NEED FOR BEFORE BUYING A PROPERTY\r\n\r\nBuying a property is a thing of great importance. It is about a person\'s shelter and is among the necessities of life. There is an array of things that you should keep in your mind while availing a rented property. You can ask an expert or a professional about the best property. You should also get to know the landlord well, the neighbors as well as the various aspects of commuting to and from the place on a regular basis. You will have to ensure that you have all the important and relevant documents before buying a property. What Are The Documents You Would Need For Before Buying A Property The Various Documents Required For Buying A Property Proof Of Employment – If you are going to buy a property for the very first time, then you will need to ensure that you have all the details. So, to know all the facts about bashar ibrahim, you will have to check the portal properly. You will have to bring your appointment letter as proof of work. It is very significant as you have the option of expressing the fact that you have a source of income to pay the rent from time to time. Photo Identification – The photo identity is among the most important documents. The document definitely varies from one to another. Additionally, it also can be any government recognized document, which has the photograph on it along with all the various other information. Most Recent Tax Return – The tax returns are known as one of the important at the time of getting a rented property. You will need to have the most recent tax returns document in original as well as an attested copy of the copy of the document. It is one of the significant document without that you will not be given residence anywhere. All The Significant Banking Data And Information When you are going to buy the property, you will have to ensure that you have all the noteworthy details of buying process. The bank data and information should be provided. You will have to make sure that you offer the bank details to the landlord. You will also have to bring the entire reference letter from the previous residing owner as to what kind of a tenant you are, and the message is written only when you have a good relationship with the previous owner. Try to know the facts about bashar Ibrahim for the detail. Not everyone is well aware of the property buying process and that is why; you must know the whole details. If you are not a resident of the country and you want to buy the property, you will be required to have a proof of residence. You can also ask an expert about this.\r\n\r\nCard image\r\nCard image\r\nCard image\r\nExperts around the world have opined that real estate is an investment that will never go sour, and for good reason. With an excellent rate of returns and unparalleled wealth-building opportunities, real estate truly is a good investment. If you are wondering whether you should finally take the leap and invest in real estate projects in Pune then this article is for you. Let’s examine some of the biggest advantages to investing in real estate, and how, when done right, investment in real estate can be highly beneficial in the long run.\r\n\r\nWhether you own a commercial office space or a luxury apartment, the inherent value of real estate is that it is always counted as a tangible asset. These assets are flexible and can be converted to suit the need of the hour, generating rental revenue for you all year round. The property will always have value, giving you returns even if you decide to sell it. Definitely oOpt for home insurance and be sure to do your research, and pick the best policy for your needs, in order to keep your assets well protected.\r\n\r\nA large majority of people invest in real estate properties for the steady cash inflow that it generates through rental income. This passive income source serves as a strong incentive for people to invest in real estate property. Metropolitan cities or urban towns with good social infrastructure and connectivity to schools, colleges and IT hubs tend to reap a higher income for the owners as the demand is always high in such areas. Location is a key component to keep in mind when investing in real estate.', '1', 'published', '1632909191.png', '/blogs', '2021-09-29 04:23:11', '2021-09-29 04:23:11');

-- --------------------------------------------------------

--
-- Table structure for table `blog_cats`
--

CREATE TABLE `blog_cats` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blog_cats`
--

INSERT INTO `blog_cats` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Property Suggestion', 'active', '2020-07-31 09:54:17', '2021-09-29 04:19:05'),
(2, 'Commercial Property', 'active', '2020-07-31 09:54:40', '2021-09-29 04:19:45'),
(3, 'Residential Property', 'active', '2020-07-31 09:54:50', '2021-09-29 04:19:51');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dribbble` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `address`, `phone`, `country`, `date_of_birth`, `role`, `status`, `gender`, `image`, `google`, `facebook`, `twitter`, `linkedin`, `skype`, `dribbble`, `remember_token`, `created_at`, `updated_at`) VALUES
(70, 'customer', '123', 'customer123', 'customer@farazisoft.com', '$2y$10$tTSsbjlAFJBGlRI0H5.2yeJMAP7feDHcASke3CUiHEiGu7lXMITHK', 'test purpoe', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '2019-01-12 05:57:53', '2019-01-12 05:57:53'),
(71, 'kaml', 'Farazi', 'kamalfarazi', 'kamal@gmail.com', '$2y$10$RLOX7oG0n7JDuih84cESTe9yxDdwL4oWA9HgVwPul2GU2N1hILm.i', 'Dhaka, Bangladesh', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '2019-01-13 11:34:37', '2019-01-13 11:34:37');

-- --------------------------------------------------------

--
-- Table structure for table `featured_amenities`
--

CREATE TABLE `featured_amenities` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `featured_amenities`
--

INSERT INTO `featured_amenities` (`id`, `prop_id`, `title`, `position`, `img`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(3, '8', 'Swimming Pool', '1', '1599555533.jpg', '/property/amenities', 'Active', '2020-09-08 03:58:53', '2020-09-08 03:58:53'),
(4, '8', 'Fitness center', '2', '1599555657.jpg', '/property/amenities', 'Active', '2020-09-08 04:00:57', '2020-09-08 04:00:57'),
(5, '8', 'Yoga Deck', '3', '1599555691.jpg', '/property/amenities', 'Active', '2020-09-08 04:01:31', '2020-09-08 04:01:31');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(10) UNSIGNED NOT NULL,
  `feature_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `feature_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Balcony', 'active', NULL, NULL),
(2, 'Gym/workout room', 'active', NULL, NULL),
(4, 'Media room', 'active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `galaries`
--

CREATE TABLE `galaries` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `galaries`
--

INSERT INTO `galaries` (`id`, `prop_id`, `name`, `original_name`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(4, '6', '1596793917.png', 'house_PNG63.png', '/galary', 'Active', '2020-08-07 04:51:57', '2020-08-07 04:51:57'),
(5, '7', '1632550194.jpg', 'Project-Photo-15-Nyati-Evara-1-Pune-5101717_600_900.jpg', '/galary', 'Active', '2020-08-08 00:42:20', '2021-09-25 00:39:54'),
(6, '8', '1632550127.jpg', 'evara top view 1.jpg', '/galary', 'Active', '2020-08-08 00:58:21', '2021-09-25 00:38:47'),
(7, '13', '1599899435.jpg', '5cbadbf7101b22072c79bc31429b1223.jpg', '/galary', 'Active', '2020-09-12 03:30:35', '2020-09-12 03:30:35'),
(8, '8', '1632550164.jpg', 'evara top view.jpg', '/galary', 'Active', '2021-09-25 00:39:06', '2021-09-25 00:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `prop_id`, `heading`, `title`, `img`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(2, '8', 'Construction Update Tower\'s G, H, I & Clubhouse', 'Clubhouse & Tower GHI', '1599927618.jpg', '/property/gallery', 'Active', '2020-09-12 11:20:18', '2020-09-12 11:20:18'),
(3, '8', 'Construction Update Tower\'s G, H, I & Clubhouse', 'Night Elevation Front', '1599927666.jpg', '/property/gallery', 'Active', '2020-09-12 11:21:06', '2020-09-12 11:21:06'),
(4, '8', 'Construction Update Tower\'s G, H, I & Clubhouse', 'Parking Entry', '1599927666.jpg', '/property/gallery', 'Active', '2020-09-12 11:21:06', '2020-09-12 11:21:06'),
(5, '8', 'Construction Update Tower\'s G, H, I & Clubhouse', 'Parking Tower E', '1599927618.jpg', '/property/gallery', 'Active', '2020-09-12 11:21:06', '2020-09-12 11:21:06'),
(6, '11', '3.5 BHK Family', 'Master Bedroom', '1599928149.jpg', '/property/gallery', 'Active', '2020-09-12 11:29:09', '2020-09-12 11:29:09'),
(7, '11', '3 BHK Family', 'Bedroom', '1599928372.jpg', '/property/gallery', 'Active', '2020-09-12 11:32:52', '2020-09-12 11:32:52'),
(8, '11', '3 BHK Family', 'Bathroom', '1599928484.jpg', '/property/gallery', 'Active', '2020-09-12 11:34:44', '2020-09-12 11:34:44'),
(9, '11', '3.5 BHK Family', 'Living Room', '1599928547.jpg', '/property/gallery', 'Active', '2020-09-12 11:35:47', '2020-09-12 11:35:47');

-- --------------------------------------------------------

--
-- Table structure for table `get_in_touches`
--

CREATE TABLE `get_in_touches` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feature_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `query` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `get_in_touches`
--

INSERT INTO `get_in_touches` (`id`, `prop_id`, `feature_name`, `name`, `email`, `city`, `mobile`, `query`, `status`, `created_at`, `updated_at`) VALUES
(1, '8', 'home', 'Aakash Kothule', 'aakash@puretechnology.in', '', '8830705470', 'Need more properties in low range.', '', '2020-09-14 01:05:03', '2020-09-14 01:05:03'),
(2, '8', 'home', 'Aakash Kothule', 'aakash@puretechnology.in', '', '8830705470', 'Need more properties in low range.', 'Active', '2020-09-14 01:45:17', '2020-09-14 01:45:17'),
(3, '8', 'home', 'Aakash Kothule', 'aakash@puretechnology.in', '', '8830705472', 'Need more properties in low range.', 'Active', '2020-09-14 03:17:05', '2020-09-14 03:17:05'),
(4, '8', 'home', 'Aakash Kothule', 'aakash@puretechnology.in', '', '8830705472', 'Need more properties in low range.', 'Active', '2020-09-14 03:17:52', '2020-09-14 03:17:52'),
(5, '8', 'home', 'Aakash Kothule', 'aakash@puretechnology.in', '', '8830705472', 'Need more properties in low range.', 'Active', '2020-09-14 03:31:58', '2020-09-14 03:31:58');

-- --------------------------------------------------------

--
-- Table structure for table `interior_features`
--

CREATE TABLE `interior_features` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feature_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `interior_features`
--

INSERT INTO `interior_features` (`id`, `prop_id`, `feature_name`, `title`, `description`, `img`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(2, '8', 'Living Room', 'LIVING ROOM', 'The Living Area is separate from the Dining Area and flows into an exterior viewing deck.', '1599482080.jpg', '/property/features', 'Active', '2020-09-07 07:34:40', '2020-09-07 07:34:40'),
(3, '8', 'MASTER BEDROOM', 'MASTER BEDROOM', 'Spacious bedrooms are a signature of the brand. Minimalist colour themes with mood lighting and ample natural lighting make the bedrooms inspiring and harmonious.', '1599482170.jpg', '/property/features', 'Active', '2020-09-07 07:36:10', '2020-09-07 07:36:10'),
(4, '8', 'KITCHEN', 'KITCHEN', 'Kitchen Cabinets: For a modern look, you need to make your appliances look built-in. Especially your refrigerator, since it occupies the highest space in the kitchen. A kitchen also needs space to store food, cookware and small appliances. Design the cabi', '1599927471.jpg', '/property/features', 'Active', '2020-09-12 11:17:51', '2020-09-12 11:17:51');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `foldername` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `languagename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `flag_image` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `foldername`, `languagename`, `description`, `flag_image`, `created_at`, `updated_at`) VALUES
(1, 'en', 'English', 'English', 'america.png', '2016-08-15 10:26:26', '2016-08-15 10:26:26'),
(22, 'bn', 'bengali', 'Bangla', '1475429828.png', '2016-10-02 11:37:08', '2016-10-02 11:37:08');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `location_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `location_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Hinjewadi', 'active', NULL, NULL),
(5, 'Undri', 'active', '2020-07-31 01:05:25', '2021-09-21 23:19:29');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `title`, `img`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(6, 'Give it up for these gen-next leaders!', '1632228173.png', '/media/Awards', 'Active', '2021-09-21 07:12:53', '2021-09-21 07:12:53');

-- --------------------------------------------------------

--
-- Table structure for table `mediakit_cats`
--

CREATE TABLE `mediakit_cats` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mediakit_cats`
--

INSERT INTO `mediakit_cats` (`id`, `name`, `icon`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Corporate', 'download_icon.png', 'Active', '2020-07-31 09:54:17', '2020-08-07 00:39:01'),
(2, 'Logo', 'image_icon.png', 'Active', '2020-07-31 09:54:40', '2020-07-31 09:54:40');

-- --------------------------------------------------------

--
-- Table structure for table `media_kit`
--

CREATE TABLE `media_kit` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cat_id` int(11) NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media_kit`
--

INSERT INTO `media_kit` (`id`, `title`, `img`, `pdf`, `cat_id`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Floor Plan', '', 'abcd.pdf', 1, '/media/MediaKit', 'Active', '2020-07-31 09:54:17', '2021-09-24 23:27:51');

-- --------------------------------------------------------

--
-- Table structure for table `media_press`
--

CREATE TABLE `media_press` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `headline` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `cha_mag_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media_press`
--

INSERT INTO `media_press` (`id`, `title`, `img`, `headline`, `cha_mag_name`, `url`, `pdf`, `date`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Times Group Award', '1632221094.png', 'Give it up for these gen-next leaders!', 'Times Group Award', NULL, NULL, '26 Feb 2020', '/media/Press', 'Active', '2021-09-21 05:14:54', '2021-09-21 06:50:14');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `receiver_id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `replay_id` int(11) UNSIGNED NOT NULL,
  `subject` text CHARACTER SET utf8,
  `description` text CHARACTER SET utf8,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `receiver_id`, `sender_id`, `replay_id`, `subject`, `description`, `created_at`, `updated_at`, `status`) VALUES
(1, 68, 61, 0, 'dsds', 'ddsd', '2016-12-24 10:03:59', '2016-12-24 10:03:59', 0),
(2, 61, 68, 0, 'trtrt', 'trtrtrt', '2016-12-28 08:57:29', '2016-12-28 08:57:29', 1),
(3, 68, 68, 0, 'trtrt', 'trtrtrt', '2016-12-28 08:57:29', '2016-12-28 08:57:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2020_07_24_122428_create_properties_table', 1),
('2020_07_24_125028_create_locations_table', 1),
('2020_07_24_125110_create_features_table', 1),
('2020_07_24_125144_create_property_types_table', 1),
('2020_07_28_115332_create_blogs_table', 2),
('2020_07_31_091628_create_blog_categories_table', 3),
('2020_07_31_135652_create_blog_cats_table', 4),
('2020_08_01_062521_create_sliders_table', 5),
('2020_08_05_135352_create_galaries_table', 6),
('2020_08_28_075942_create_property_interiors_table', 7),
('2020_08_28_080410_create_interior_features_table', 7),
('2020_08_28_080501_create_get_in_touches_table', 7),
('2020_08_28_084226_create_technology_features_table', 8),
('2020_08_28_084431_create_featured_amenities_table', 8),
('2020_08_31_083443_create_contacts_table', 8),
('2020_09_06_064005_create_property_plans_table', 9),
('2020_09_06_064911_create_plan_features_table', 9),
('2020_09_07_040011_create_plan_views_table', 10),
('2020_09_08_115824_create_galleries_table', 11),
('2020_09_09_034447_create_media_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `removable`, `created_at`, `updated_at`) VALUES
(1, 'users.manage', 'Manage Users', 'Manage users and their sessions.', 0, '2016-06-17 21:58:04', '2016-06-17 21:58:04'),
(2, 'languages.languages', 'Language manage', 'View languages', 0, '2016-06-17 21:58:04', '2016-09-24 09:28:12'),
(3, 'roles.manage', 'Manage Roles', 'Manage system roles.', 0, '2016-06-17 21:58:04', '2016-06-17 21:58:04'),
(4, 'permissions.manage', 'Manage Permissions', 'Manage role permissions.', 0, '2016-06-17 21:58:04', '2016-06-17 21:58:04'),
(5, 'users.activity', 'Activity', 'View users activity', 1, '2016-09-24 09:47:25', '2016-09-24 09:47:25'),
(7, 'message.messages', 'Message', 'View inbox send message', 0, '2016-06-17 21:58:04', '2016-09-24 09:31:04'),
(8, 'settings.general', 'Settings', 'Update system settings.', 0, '2016-06-17 21:58:04', '2016-09-24 09:30:15'),
(9, 'customer.customers', 'Customer', 'Customer', 1, '2019-01-14 08:04:47', '2019-01-14 08:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(7, 1),
(8, 1),
(9, 1),
(7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `plan_features`
--

CREATE TABLE `plan_features` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prop_plan_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_features`
--

INSERT INTO `plan_features` (`id`, `prop_id`, `prop_plan_id`, `title`, `img`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(2, '8', '3', 'Living Room', '1599409737.jpg', '/property/plan/feature', 'Active', '2020-09-06 11:28:57', '2020-09-06 11:28:57'),
(3, '8', '3', 'Master Nedroom', '1599907036.jpg', '/property/plan/feature	', 'Active', '2020-09-12 05:37:16', '2020-09-12 05:37:16'),
(4, '8', '3', 'Dining Area', '1599930302.jpg', '/property/plan/feature	', 'Active', '2020-09-12 12:05:02', '2020-09-12 12:05:02');

-- --------------------------------------------------------

--
-- Table structure for table `plan_views`
--

CREATE TABLE `plan_views` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_views`
--

INSERT INTO `plan_views` (`id`, `prop_id`, `title`, `img`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(1, '8', 'ARIAL VIEW', '1599565781.jpg', '/property/plan/view', 'Active', '2020-09-08 06:49:41', '2020-09-08 06:49:41'),
(2, '8', 'CLUBHOUSE', '1599929841.jpg', '/property/plan/view', 'Active', '2020-09-12 11:57:21', '2020-09-12 11:57:21'),
(3, '8', 'MASTER PLAN', '1599930056.png', '/property/plan/view', 'Active', '2020-09-12 12:00:56', '2020-09-12 12:00:56');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `prop_id` int(10) UNSIGNED NOT NULL,
  `property_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year_built` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rera_number` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `square_feet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_per_square_feet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `offer_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `features` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feature_folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `master_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `master_folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`prop_id`, `property_name`, `type`, `year_built`, `bed`, `bath`, `rera_number`, `square_feet`, `price`, `price_per_square_feet`, `status`, `description`, `city`, `location`, `featured`, `offer_type`, `features`, `feature_folder`, `featured_image`, `master_image`, `master_folder`, `created_at`, `updated_at`) VALUES
(7, 'Evara - I at Undri Pune', '2', '2002', '2', '1', 'P52100000455', '13988', '65,00,000', '5198', 'Active', 'Avalon Evara, situated in the lush green vicinity of Nyati County, a neighborhood of gated communities. It is located in the well-connected suburb of Undri, Pune, nestled amidst quiet and beautiful surroundings. A project that beautifully integrates all aspects of luxury, elegance, value and comfort in its 2 & 3 RHK apartments. Evara offers a boutique lifestyle with amenities that elevate the quality of your life. Own a home and live a dream life!', 'Pune', '1', 'yes', 'sale', 'Balcony,Gym/workout room,Media room', '/property', '1632227832.jpg', '1632227832.jpg', '/property/master', '2020-08-07 05:17:13', '2021-09-21 07:07:12'),
(8, 'Evara - 2 at Undri Pune', '3', '2009', '2', '3', 'P52100021346', '13988', '57,00,000', '5198', 'Active', 'Avalon Evara, situated in the lush green vicinity of Nyati County, a neighborhood of gated communities. It is located in the well-connected suburb of Undri, Pune, nestled amidst quiet and beautiful surroundings. A project that beautifully integrates all aspects of luxury, elegance, value and comfort in its 2 & 3 RHK apartments. Evara offers a boutique lifestyle with amenities that elevate the quality of your life. Own a home and live a dream life!', 'Pune', '5', 'yes', 'sale', 'Balcony,Gym/workout room,Media room', '/property', '1632228026.jpg', '1632228026.jpg', '/property/master', '2020-08-08 00:55:13', '2021-10-20 02:28:39');

-- --------------------------------------------------------

--
-- Table structure for table `property_interiors`
--

CREATE TABLE `property_interiors` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `property_interiors`
--

INSERT INTO `property_interiors` (`id`, `prop_id`, `title`, `img`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(3, '8', '3 BHK Trendy', '1599459845.jpg', '/property/HomeType', 'Active', '2020-09-07 01:24:05', '2020-09-07 01:24:05'),
(4, '8', '3.5 BHK Family', '1599924514.jpg', '', 'Active', '2020-09-12 10:28:34', '2020-09-12 10:28:34'),
(5, '8', '4 BHK', '1599924577.jpg', '', 'Active', '2020-09-12 10:29:37', '2020-09-12 10:29:37');

-- --------------------------------------------------------

--
-- Table structure for table `property_plans`
--

CREATE TABLE `property_plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_in` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `property_plans`
--

INSERT INTO `property_plans` (`id`, `prop_id`, `title`, `area`, `price`, `price_in`, `img`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(3, '8', 'EVEN_FLOOR_A1', '137.76 - 142.44 SQ. M.', '58', 'Lakhs Rupees', '1632227922.jpg', '/property/plan', 'Active', '2020-09-06 09:44:15', '2021-09-21 07:08:42'),
(4, '8', 'ODD_FLOOR_A1', '172.55 - 177.99 SQ. M.', '59', 'Lakhs Rupees', '1632227902.jpg', '/property/plan', 'Active', '2020-09-08 04:21:57', '2021-09-21 07:08:54');

-- --------------------------------------------------------

--
-- Table structure for table `property_types`
--

CREATE TABLE `property_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `property_types`
--

INSERT INTO `property_types` (`id`, `type_name`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Commercial Building', 'active', '2020-08-07 04:26:14', '2021-09-21 23:18:50'),
(3, 'Residential Property', 'active', '2020-08-07 04:26:30', '2021-09-21 23:18:42'),
(4, 'Office space', 'active', '2020-08-07 04:27:28', '2021-09-21 23:19:09');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `removable`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'System administrator.', 0, '2016-06-17 21:58:04', '2016-06-17 21:58:04'),
(2, 'Employee', 'Employee', 'Default system user list.', 0, '2016-06-17 21:58:04', '2019-01-10 02:40:27'),
(3, 'Customer', 'Customer', '', 1, '2019-01-10 02:40:41', '2019-01-10 02:40:41');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(61, 1),
(68, 2);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_me` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forget_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notify_signup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `re_capcha` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `app_name`, `app_title`, `app_email`, `phone`, `mobile`, `currency`, `remember_me`, `forget_password`, `notify_signup`, `re_capcha`, `logo`, `address`, `created_at`, `updated_at`) VALUES
(10, 'Indulkar', 'Dipak', 'it@indulkargroup.com', '7721008844', '7721008844', '$', 'ON', 'ON', 'OFF', 'ON', '1632226761.jpg', 'Survey No. 41/B2,\r\nDiamond Water Park Road,\r\nLohegaon,', '2016-09-01 19:38:54', '2021-09-29 02:10:21');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `prop_id`, `title`, `description`, `image`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(2, 7, 'Evara -1 at Undri', 'Avalon Evara, situated in the lush green vicinity of Nyati County, a neighborhood of gated communities. It is located in the well-connected suburb of Undri, Pune, nestled amidst quiet and beautiful surroundings. A project that beautifully integrates all a', '1596561230.png', '/slider', 'Active', '2020-08-04 12:13:50', '2021-09-25 00:21:40'),
(6, 8, 'Undri - Evara - 2', 'Avalon Evara, situated in the lush green vicinity of Nyati County, a neighborhood of gated communities. It is located in the well-connected suburb of Undri, Pune, nestled amidst quiet and beautiful surroundings. A project that beautifully integrates all a', '1604736789.jpg', '/slider', 'Active', '2020-11-07 03:13:10', '2021-10-20 02:30:17');

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `provider_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `technology_features`
--

CREATE TABLE `technology_features` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `technology_features`
--

INSERT INTO `technology_features` (`id`, `prop_id`, `title`, `description`, `img`, `folder`, `status`, `created_at`, `updated_at`) VALUES
(2, '8', '3-IN-1 SECURITY', '3-in-1 door locking system that includes access via key, password, and biometrics, to ensure complete safety.', '1599484287.jpg', '/property/technology', 'Active', '2020-09-07 08:11:27', '2020-09-07 08:11:27'),
(3, '8', 'LOW EMISSION GLASS', 'Use of Double glazed Low-Emission glass windows with better energy savings.', '1599484885.jpg', '/property/technology', 'Active', '2020-09-07 08:21:25', '2020-09-07 08:21:25'),
(4, '8', 'CENTRALIZED AC', 'Centralised Air Conditioning with VRF cooling.', '1599485040.jpg', '/property/technology', 'Active', '2020-09-07 08:24:00', '2020-09-07 08:24:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dribbble` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `address`, `phone`, `country`, `date_of_birth`, `role`, `status`, `gender`, `image`, `google`, `facebook`, `twitter`, `linkedin`, `skype`, `dribbble`, `remember_token`, `created_at`, `updated_at`) VALUES
(61, 'Dipak', 'Indulkar Group', 'Aakash', 'indulkar@support.com', '$2y$10$jJZEFwH0WPZtScoXhS4hqeL4VM2xJ7XTJXVUFBZ5ceAgKrql1CMDC', 'Pune', '7721008844', 'India', '06/03/1986', 'Admin', 'Active', 'Male', '1480345486.png', 'https://www.google.com/', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.linkedin.com/', 'jewel_farazi', 'https://dribbble.com/', 'dRfvn6MC7Z2sKalesYqMOSclSwChWvxbQHKiXj9BYfq3vjKErl5f8BAfxJKg', '2016-10-22 12:03:26', '2021-09-29 02:11:33'),
(68, 'user', '123', 'user123', 'user@farazisfot.com', '$2y$10$NhdbCkl2Ir4K.gPkOf9ICegZw72LRkF5mTRiizpkVC6i.cx4cgCge', 'dsdd', '01745519614', 'Afghanistan', '2016-09-28', 'Employee', 'Active', 'Male', '1482937747.png', 'https://www.google.com/', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.linkedin.com/', 'jewel_farazi', 'https://dribbble.com/', '8qUoujk1lVArFxBVPMBaCX5dZWMLdR5wph0Tk3v3M4CkzXeGd6j337UVdB2w', '2016-10-22 12:03:26', '2019-01-10 02:41:11');

-- --------------------------------------------------------

--
-- Table structure for table `user_activity`
--

CREATE TABLE `user_activity` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user_activity`
--

INSERT INTO `user_activity` (`id`, `description`, `user_id`, `ip_address`, `user_agent`, `created_at`, `updated_at`) VALUES
(1, 'Role Updated.', 61, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-10 08:40:28', '2019-01-10 08:40:28'),
(7, 'User Updated.', 61, '157.32.116.9', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', '2020-07-27 17:39:54', '2020-07-27 17:39:54'),
(8, 'User Updated.', 61, '157.32.116.9', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', '2020-07-27 17:42:12', '2020-07-27 17:42:12'),
(9, 'User Updated.', 61, '103.239.85.42', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', '2021-09-21 12:19:21', '2021-09-21 12:19:21'),
(10, 'User Updated.', 61, '103.239.85.42', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', '2021-09-29 07:41:33', '2021-09-29 07:41:33'),
(11, 'User Updated.', 61, '103.239.85.42', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', '2021-09-29 07:42:16', '2021-09-29 07:42:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_cats`
--
ALTER TABLE `blog_cats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `featured_amenities`
--
ALTER TABLE `featured_amenities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `featured_amenities_prop_id_index` (`prop_id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galaries`
--
ALTER TABLE `galaries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galaries_prop_id_index` (`prop_id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_prop_id_index` (`prop_id`);

--
-- Indexes for table `get_in_touches`
--
ALTER TABLE `get_in_touches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `get_in_touches_prop_id_index` (`prop_id`);

--
-- Indexes for table `interior_features`
--
ALTER TABLE `interior_features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `interior_features_prop_id_index` (`prop_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mediakit_cats`
--
ALTER TABLE `mediakit_cats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_kit`
--
ALTER TABLE `media_kit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_press`
--
ALTER TABLE `media_press`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`receiver_id`),
  ADD KEY `FK_message_user_2` (`sender_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `plan_features`
--
ALTER TABLE `plan_features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_features_prop_id_index` (`prop_id`);

--
-- Indexes for table `plan_views`
--
ALTER TABLE `plan_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_views_prop_id_index` (`prop_id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`prop_id`);

--
-- Indexes for table `property_interiors`
--
ALTER TABLE `property_interiors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_interiors_prop_id_index` (`prop_id`);

--
-- Indexes for table `property_plans`
--
ALTER TABLE `property_plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_plans_prop_id_index` (`prop_id`);

--
-- Indexes for table `property_types`
--
ALTER TABLE `property_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `technology_features`
--
ALTER TABLE `technology_features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `technology_features_prop_id_index` (`prop_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_activity`
--
ALTER TABLE `user_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_activity_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `blog_cats`
--
ALTER TABLE `blog_cats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `featured_amenities`
--
ALTER TABLE `featured_amenities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `galaries`
--
ALTER TABLE `galaries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `get_in_touches`
--
ALTER TABLE `get_in_touches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `interior_features`
--
ALTER TABLE `interior_features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mediakit_cats`
--
ALTER TABLE `mediakit_cats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `media_kit`
--
ALTER TABLE `media_kit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `media_press`
--
ALTER TABLE `media_press`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `plan_features`
--
ALTER TABLE `plan_features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `plan_views`
--
ALTER TABLE `plan_views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `prop_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `property_interiors`
--
ALTER TABLE `property_interiors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `property_plans`
--
ALTER TABLE `property_plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `property_types`
--
ALTER TABLE `property_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `technology_features`
--
ALTER TABLE `technology_features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `user_activity`
--
ALTER TABLE `user_activity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `FK_message_users` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_message_users_2` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD CONSTRAINT `social_accounts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_activity`
--
ALTER TABLE `user_activity`
  ADD CONSTRAINT `user_activity_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
